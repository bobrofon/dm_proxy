package wget

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

const (
	listCommand   = "screen -list | grep dmproxy | sed -r 's/^.*dmproxy.//' | awk '{print $1;}'"
	statusCommand = "screen -S dmproxy.%s -X hardcopy /tmp/dmproxy_buf.txt && cat /tmp/dmproxy_buf.txt | grep -v '^$' | tail -n 1 | sed 's/\\.\\.//g' | sed -r 's/\\s+/ /g' && rm -rf /tmp/dmproxy_buf.txt > /dev/null 2>&1"
	addCommand    = "screen -S dmproxy.%s -dm bash -c \"wget --progress=dot '%s' -O /media/sg/downloads/%s 2>&1 | grep --line-buffered '%%'\""
	getPid        = "screen -list | grep dmproxy.%s | sed 's/\\..*$//g' | sed 's/\\s//g'"
)

func Add(url string, fileName string) error {
	command := fmt.Sprintf(addCommand, fileName, url, fileName)
	err := exec.Command("bash", "-c", command).Run()

	return err
}

func getFileList() ([]string, error) {
	out, err := exec.Command("bash", "-c", listCommand).CombinedOutput()
	if err != nil {
		return nil, err
	}
	outString := strings.Trim(string(out), " \n\t")
	fileNameList := strings.Split(outString, "\n")
	return fileNameList, nil
}

func getFileStatus(fileName string) (string, error) {
	command := fmt.Sprintf(statusCommand, fileName)
	out, err := exec.Command("bash", "-c", command).CombinedOutput()
	outString := strings.Trim(string(out), " \n\t")
	return outString, err
}

type FileStatus struct {
	Name   string
	Status string
}

func GetStatus() ([]FileStatus, error) {
	fileNameList, err := getFileList()
	if err != nil {
		return nil, err
	}
	status := make([]FileStatus, len(fileNameList))
	errorList := make([]string, 0, len(status))
	for i, fileName := range fileNameList {
		fileStat, err := getFileStatus(fileName)
		status[i].Name = fileName
		status[i].Status = fileStat
		if err != nil {
			errorList = append(errorList, err.Error())
		}
	}

	if len(errorList) > 0 {
		errorString := strings.Join(errorList, "\n")
		return status, errors.New(errorString)
	} else {
		return status, nil
	}
}

func getPidByFileName(fileName string) (int, error) {
	command := fmt.Sprintf(getPid, fileName)
	out, err := exec.Command("bash", "-c", command).CombinedOutput()
	outString := strings.Trim(string(out), " \n\t")
	pid, err := strconv.Atoi(outString)
	return pid, err
}

func getFullPath(fileName string) string {
	return "/media/sg/" + fileName
}

func Delete(fileName string) error {
	pid, err := getPidByFileName(fileName)
	if err != nil {
		return err
	}
	process, err := os.FindProcess(pid)
	if err != nil {
		return err
	}
	err = process.Kill()
	if err != nil {
		return err
	}
	err = os.Remove(getFullPath(fileName))
	return err
}
