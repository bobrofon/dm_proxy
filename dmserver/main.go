package main

import (
	"dmproxy/wget"
	"encoding/json"
	"flag"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"strings"
)

func main() {
	var port int
	flag.IntVar(&port, "port", 4242, "server port")
	flag.Parse()

	http.HandleFunc("/add", add)
	http.HandleFunc("/list", getList)
	http.HandleFunc("/del", del)
	http.ListenAndServe(":"+strconv.Itoa(port), nil)
}

func add(w http.ResponseWriter, r *http.Request) {
	urlString := r.FormValue("url")
	url, err := url.Parse(urlString)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, fileName := path.Split(url.Path)
	if fileName == "" {
		http.Error(w, "Invalid path "+url.Path, http.StatusBadRequest)
		return
	}
	fileName = strings.Replace(fileName, " ", "_", -1)

	err = wget.Add(urlString, fileName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func getList(w http.ResponseWriter, r *http.Request) {
	statusList, err := wget.GetStatus()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err := json.Marshal(statusList)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func del(w http.ResponseWriter, r *http.Request) {
	fileName := r.FormValue("fileName")

	err := wget.Delete(fileName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
