function addNewLink (form) {
	var urlVar = form.url.value;
	form.url.value = "";
	
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			statusUpdate();
		}
	}

	xmlhttp.open("GET", "/dm/action/add?url=" + encodeURIComponent(urlVar), true);
	xmlhttp.send();
}

function cancelFileDownload(fileName) {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			statusUpdate();
		}
	}

	xmlhttp.open("GET", "/dm/action/del?file=" + encodeURIComponent(fileName), true);
	xmlhttp.send();
}

function statusArrayToTable(statusArray) {
	var table = []
	for (var stat of statusArray) {
		table.push("<tr class=row><td class=col-3>", stat.Name, "</td><td class=col-3>", stat.Status,
		"</td><td class=col-3><button onclick=\"cancelFileDownload('", stat.Name,"')\">cancel</button></td></tr>");
	}
	return table.join("");
}

function replaceTable(tableData) {
	var tableHtml = statusArrayToTable(tableData);
	var table = document.getElementById("status_table");
	table.innerHTML = tableHtml;
}

function statusUpdate() {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var tableData;
			try {
				tableData = JSON.parse(xmlhttp.responseText);
			} catch(e) {
				console.log(e.message);
				return;
			}
			replaceTable(tableData);
		}
	}
	xmlhttp.open("GET", "/dm/action/list", true);
	xmlhttp.send();
}

function startStatusUpdateTimer() {
	statusUpdate();
	setInterval(statusUpdate, 3000);
}

function init() {
	window.onload = startStatusUpdateTimer;
}

init();
