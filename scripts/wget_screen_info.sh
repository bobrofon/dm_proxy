#! /bin/bash

screen -S olo -X hardcopy buf.tmp > /dev/null 2>&1 && cat `pwd`/buf.tmp | grep -v '^$' | tail -n 1 | sed 's/\.//g' | sed -r 's/\s+/ /g' && rm -rf `pwd`/buf.tmp > /dev/null 2>&1
